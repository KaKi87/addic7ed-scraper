const axios = require('axios').default;

const { regexpMatchSubgroups } = require('./utils');

const rootUrl = 'https://www.addic7ed.com';

const parsePage = (body, type) => ({
    [`${type}Id`]: parseInt(body.match(/var subID='([0-9]+)'/)[1]),
    name: body.match(/titulo">\s+(.+?) <s/)[1],
    picture: type === 'show' ? body.match(/https:\/\/www\.addic7ed\.com\/images\/showimages\/.+?\.jpg/)[0] : undefined,
    releases: regexpMatchSubgroups(
        body,
        /Version ([^,]+).+?\/user\/([0-9]+)'>([^<]+)<\/a> {2}([0-9]+) (minutes|hours|days)(.+?)<\/table>/gs
    ).map(releaseMatch => {
        const languages = regexpMatchSubgroups(
            releaseMatch[6],
            /language">([^<]+).+?<b>([0-9.]+)?(% )?Completed.+?href="\/([^"]+).+?([0-9+]) times edited · ([0-9]+) Downloads · ([0-9]+) sequences(?:.{290}?edited {2}([0-9]+) (minutes|hours|days))?/gs
        ).map(languageMatch => ({
            language: languageMatch[1],
            progressPercent: languageMatch[2] ? parseFloat(languageMatch[2]) : 100,
            downloadUrl: `${rootUrl}/${languageMatch[4]}`,
            editCount: parseInt(languageMatch[5]),
            downloadCount: parseInt(languageMatch[6]),
            sequenceCount: parseInt(languageMatch[7]),
            editedMinutesAgo: languageMatch[9] === 'minutes' ? parseInt(languageMatch[8]) : undefined,
            editedHoursAgo: languageMatch[9] === 'hours' ? parseInt(languageMatch[8]) : undefined,
            editedDaysAgo: languageMatch[9] === 'days' ? parseInt(languageMatch[8]) : undefined
        }));
        return {
            releaseName: releaseMatch[1],
            userId: parseInt(releaseMatch[2]),
            userName: releaseMatch[3],
            uploadedMinutesAgo: releaseMatch[5] === 'minutes' ? parseInt(releaseMatch[4]) : undefined,
            uploadedHoursAgo: releaseMatch[5] === 'hours' ? parseInt(releaseMatch[4]) : undefined,
            uploadedDaysAgo: releaseMatch[5] === 'days' ? parseInt(releaseMatch[4]) : undefined,
            ...languages[0],
            progressPercent: undefined,
            translations: languages.slice(1)
        };
    })
});

module.exports = {
    getShows: async () => regexpMatchSubgroups((await axios({
        url: rootUrl,
        headers: { 'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36' }
    })).data, /<option value="([0-9]+)" >([^<]+)<\/option>/g).map(match => ({
        showId: parseInt(match[1]),
        showName: match[2]
    })),
    getSeasons: async showId => regexpMatchSubgroups(
        (await axios(`${rootUrl}/ajax_getSeasons.php?showID=${showId}`)).data,
        /<option>([^<]+)<\/option>/g
    ).map(match => ({ seasonIndex: parseInt(match[1]) })),
    getEpisodes: async (showId, seasonIndex) => regexpMatchSubgroups(
        (await axios(`${rootUrl}/ajax_getEpisodes.php?showID=${showId}&season=${seasonIndex}`)).data,
        /<option value="([0-9]+)-([0-9]+)x([0-9]+)">[0-9]+\. ([^<]+)<\/option>/g
    ).map(match => ({
        showId: parseInt(match[1]),
        seasonIndex: parseInt(match[2]),
        episodeIndex: parseInt(match[3]),
        episodeName: match[4]
    })),
    getEpisodesIds: async (showId, seasonIndex) => regexpMatchSubgroups(
        (await axios(`${rootUrl}/ajax_loadShow.php?show=${showId}&season=${seasonIndex}&langs=`)).data,
        /([^\/]+)\/([0-9]+)\/([0-9]+)\/([^"]+)">([^<]+).+?href.+?([0-9]+)\/[0-9]+"/gs
    ).map(match => ({
        showSlug: match[1],
        seasonIndex: parseInt(match[2]),
        episodeIndex: parseInt(match[3]),
        episodeSlug: match[4],
        episodeName: match[5],
        episodeId: parseInt(match[6])
    })).filter(({ episodeId }, index, self) => self.findIndex(episode => episodeId === episode.episodeId) === index),
    getEpisode: async ({ showId, showSlug }, seasonIndex, episodeIndex, episodeSlug) => {
        let url = rootUrl;
        if(showId && seasonIndex && episodeIndex)
            url += `/re_episode.php?ep=${showId}-${seasonIndex}x${episodeIndex}`;
        else if(showSlug && seasonIndex && episodeIndex && episodeSlug)
            url += `/serie/${showSlug}/${seasonIndex}/${episodeIndex}/${episodeSlug}`;
        return parsePage((await axios(url)).data, 'episode');
    },
    getMovie: async movieId => parsePage((await axios(`${rootUrl}/movie/${movieId}`)).data, 'movie'),
    getComments: async episodeOrMovieId => regexpMatchSubgroups(
        (await axios(`${rootUrl}/ajax_getComments.php?id=${episodeOrMovieId}`)).data,
        /user\/([0-9]+)'>([^<]+).+?([0-9]+) (minutes|hours|days).+?#.+?>([^<]+)/gs
    ).map(match => ({
        userId: parseInt(match[1]),
        userName: match[2],
        minutesAgo: match[4] === 'minutes' ? parseInt(match[3]) : undefined,
        hoursAgo: match[4] === 'hours' ? parseInt(match[3]) : undefined,
        daysAgo: match[4] === 'days' ? parseInt(match[3]) : undefined,
        content: match[5].trim()
    })),
    getUser: async userId => {
        const match = (await axios(`${rootUrl}/user/${userId}`)).data
            .match(/&nbsp;\n([^\n]+).+gravatar_id=([^&]+).+?Since<\/td>.+?<td>([^<]+).+?site<\/td>.+?<td>([^<]*).+?Gender<\/td>.+?<td> ([^<]+).+?Class<\/td>.+?>([^<]+).+?Seen<\/td>.+?<td>([^<]+).+?Versions created \(newest 100\)<\/td>.+?<td>([^<]+).+?Episodes created \(newest 100\)<\/td>.+?<td>([^<]+).+?Episodes edited \(newest 100\)<\/td>.+?<td>([^<]+)/s);
        return {
            userName: match[1],
            picture: `https://www.gravatar.com/avatar/${match[2]}`,
            createdTimestamp: new Date(match[3]).getTime(),
            updatedTimestamp: new Date(match[7]).getTime(),
            website: match[4] || undefined,
            isMale: match[5] === 'Male',
            isFemale: match[5] === 'Female',
            isNonBinary: match[5] === 'Non-Binary',
            isMember: match[6] === 'Regular user',
            isVip: match[6] === 'VIP',
            isTeam: match[6] === 'Team Member',
            isMod: match[6] === 'Moderator',
            isAdmin: match[6] === 'Administrator',
            releaseCount: parseInt(match[8]),
            episodeCount: parseInt(match[9]),
            translationCount: parseInt(match[10])
        };
    },
    search: async query => regexpMatchSubgroups(
        (await axios(`${rootUrl}/search.php?search=${query.replace(' ', '+')}&Submit=Search`)).data,
        /"(movie|serie)\/([^"]+)".+?>([^<]+)/g
    ).map(match => {
        const res = {
            isEpisode: match[1] === 'serie',
            isMovie: match[1] === 'movie',
            name: match[3]
        };
        if(res.isEpisode){
            const
                path = match[2].split('/'),
                nameMatch = res.name.match(/^(.+) - [0-9]{2}x[0-9]{2} - (.+)$/);
            Object.assign(res, {
                showSlug: path[0],
                showName: nameMatch[1],
                seasonIndex: parseInt(path[1]),
                episodeIndex: parseInt(path[2]),
                episodeSlug: path[3],
                episodeName: nameMatch[2]
            });
        }
        else if(res.isMovie) res.movieId = parseInt(match[2]);
        return res;
    })
};