module.exports = {
    regexpMatchSubgroups: (string, regexp) => {
        const matches = [];
        let match;
        while(match = regexp.exec(string))
            matches.push(match);
        return matches;
    }
}